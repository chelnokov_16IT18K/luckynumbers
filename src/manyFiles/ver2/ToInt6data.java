package manyFiles.ver2;

import java.io.*;

/**
 * Класс, принимающий данные(числа) из файла "intdata.dat"
 * и записывающий только шестизначные в файл "int6data.dat"
 *
 * @author Chelnokov E.I.,16IT18K
 *
 */

public class ToInt6data {
    public static void main(String[] args) {
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream("src\\intdata.dat"));
             DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("src\\int6data.dat"))) {
            String data;//строка с числом
            while (dataInputStream.available() != 0) {
                data = dataInputStream.readUTF();
                String[] stillStringData = data.split("");
                if (stillStringData.length == 6) {
                    dataOutputStream.writeUTF(data);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
