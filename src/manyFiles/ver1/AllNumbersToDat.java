package manyFiles.ver1;

import java.io.*;

/**
 * Класс, принимающий данные из текстового файла
 * и записывающий в файл с расширением .dat
 * @author Chelnokov E.I., 16IT18K
 */

public class AllNumbersToDat {
    public static void main(String[] args) throws IOException {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\intdata.txt"));
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("src\\intdata.dat"))) {
            String string;//строка из файла
            while ((string = bufferedReader.readLine()) != null) {
                dataOutputStream.writeInt(Integer.valueOf(string));
            }
            bufferedReader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
